﻿namespace Run_and_Kills
{
    partial class Run_and_Kill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Background_Play = new System.Windows.Forms.Label();
            this.Backgroud_menu = new System.Windows.Forms.Panel();
            this.Button_play = new System.Windows.Forms.Button();
            this.Background_heath1 = new System.Windows.Forms.Panel();
            this.Background_heath2 = new System.Windows.Forms.Panel();
            this.Heath = new System.Windows.Forms.Panel();
            this.Jump = new System.Windows.Forms.Button();
            this.Fire = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Background_Play.SuspendLayout();
            this.Backgroud_menu.SuspendLayout();
            this.Background_heath1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Background_Play
            // 
            this.Background_Play.BackColor = System.Drawing.Color.YellowGreen;
            this.Background_Play.Controls.Add(this.Backgroud_menu);
            this.Background_Play.Location = new System.Drawing.Point(1228, 95);
            this.Background_Play.Name = "Background_Play";
            this.Background_Play.Size = new System.Drawing.Size(1080, 720);
            this.Background_Play.TabIndex = 0;
            // 
            // Backgroud_menu
            // 
            this.Backgroud_menu.BackColor = System.Drawing.Color.OliveDrab;
            this.Backgroud_menu.Controls.Add(this.Button_play);
            this.Backgroud_menu.Location = new System.Drawing.Point(238, 281);
            this.Backgroud_menu.Name = "Backgroud_menu";
            this.Backgroud_menu.Size = new System.Drawing.Size(654, 338);
            this.Backgroud_menu.TabIndex = 1;
            // 
            // Button_play
            // 
            this.Button_play.BackColor = System.Drawing.Color.Lime;
            this.Button_play.Font = new System.Drawing.Font("Segoe Script", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_play.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Button_play.Location = new System.Drawing.Point(174, 161);
            this.Button_play.Name = "Button_play";
            this.Button_play.Size = new System.Drawing.Size(312, 146);
            this.Button_play.TabIndex = 0;
            this.Button_play.Text = "Play";
            this.Button_play.UseCompatibleTextRendering = true;
            this.Button_play.UseVisualStyleBackColor = false;
            this.Button_play.Click += new System.EventHandler(this.Button_play_Click);
            // 
            // Background_heath1
            // 
            this.Background_heath1.BackColor = System.Drawing.Color.DarkGreen;
            this.Background_heath1.Controls.Add(this.Background_heath2);
            this.Background_heath1.Controls.Add(this.Heath);
            this.Background_heath1.Location = new System.Drawing.Point(308, 582);
            this.Background_heath1.Name = "Background_heath1";
            this.Background_heath1.Size = new System.Drawing.Size(470, 44);
            this.Background_heath1.TabIndex = 1;
            // 
            // Background_heath2
            // 
            this.Background_heath2.BackColor = System.Drawing.Color.OliveDrab;
            this.Background_heath2.Location = new System.Drawing.Point(16, 10);
            this.Background_heath2.Name = "Background_heath2";
            this.Background_heath2.Size = new System.Drawing.Size(440, 25);
            this.Background_heath2.TabIndex = 0;
            // 
            // Heath
            // 
            this.Heath.BackColor = System.Drawing.Color.Chartreuse;
            this.Heath.Location = new System.Drawing.Point(16, 10);
            this.Heath.Name = "Heath";
            this.Heath.Size = new System.Drawing.Size(350, 25);
            this.Heath.TabIndex = 0;
            // 
            // Jump
            // 
            this.Jump.BackColor = System.Drawing.Color.DarkGreen;
            this.Jump.Font = new System.Drawing.Font(".VnUniverse", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Jump.Location = new System.Drawing.Point(41, 572);
            this.Jump.Name = "Jump";
            this.Jump.Size = new System.Drawing.Size(200, 80);
            this.Jump.TabIndex = 2;
            this.Jump.Text = "JUMP";
            this.Jump.UseVisualStyleBackColor = false;
            // 
            // Fire
            // 
            this.Fire.BackColor = System.Drawing.Color.DarkGreen;
            this.Fire.Font = new System.Drawing.Font(".VnUniverse", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fire.Location = new System.Drawing.Point(836, 572);
            this.Fire.Name = "Fire";
            this.Fire.Size = new System.Drawing.Size(200, 80);
            this.Fire.TabIndex = 2;
            this.Fire.Text = "FIRE";
            this.Fire.UseVisualStyleBackColor = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            // 
            // Run_and_Kill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 673);
            this.Controls.Add(this.Background_Play);
            this.Controls.Add(this.Background_heath1);
            this.Controls.Add(this.Jump);
            this.Controls.Add(this.Fire);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "Run_and_Kill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Run_and_Kill";
            this.Load += new System.EventHandler(this.Run_and_Kill_Load);
            this.Background_Play.ResumeLayout(false);
            this.Backgroud_menu.ResumeLayout(false);
            this.Background_heath1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Background_Play;
        internal System.Windows.Forms.Panel Backgroud_menu;
        private System.Windows.Forms.Button Button_play;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel Background_heath1;
        private System.Windows.Forms.Panel Heath;
        private System.Windows.Forms.Panel Background_heath2;
        private System.Windows.Forms.Button Fire;
        private System.Windows.Forms.Button Jump;
    }
}

