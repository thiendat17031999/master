﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Run_and_Kills
{
    public partial class Run_and_Kill : Form
    {
        Bitmap Background_ingame1;
        Bitmap Background_ingame2;
        Bitmap Background1_ingame1;
        Bitmap Background1_ingame2;
        Bitmap Background2_ingame1;
        Bitmap Background2_ingame2;
        Bitmap Background3_ingame1;
        Bitmap Background3_ingame2;
        Bitmap BackgroundLane_ingame1;
        Bitmap BackgroundLane_ingame2;
        Rectangle Rec_background_ingame1;
        Rectangle Rec_background_ingame2;
        Rectangle Rec_background1_ingame1;
        Rectangle Rec_background1_ingame2;
        Rectangle Rec_background2_ingame1;
        Rectangle Rec_background2_ingame2;
        Rectangle Rec_background3_ingame1;
        Rectangle Rec_background3_ingame2;
        Rectangle Rec_backgroundLane_ingame1;
        Rectangle Rec_backgroundLane_ingame2;
        Bitmap All;
        int bossTimer = -1;
        class Bullet
        {
            private Bitmap img;
            private int x;
            private int y;

            public Bullet()
                {
                    img = new Bitmap(Run_and_Kills.Properties.Resources.Bullet);
                    x = -1;
                    y = -1;
                }
            public void set_location(int x, int y)
                {
                    this.x = x;
                    this.y = y;
                }
            public void increase_location()
            {
                this.x += 20;
            }
            public Bitmap get_img()
            {
                return img;
            }
            public int get_x()
            {
                return this.x;
            }
            public int get_y()
            {
                return this.y;
            }
        }
        private Bitmap sprite_run;
        private Bitmap sprite_jump;
        private Bitmap sprite_360;
        private Bullet bullet;
        Bitmap backBuffer_run;
        public Graphics graphics_run;
        private int index_sprite;
        private int curFrameColumn_run;
        private int curFrameRow_run;
        List<Bullet> bullets = new List<Bullet>();
        public Run_and_Kill()
        {
            InitializeComponent();
            All = new Bitmap(1080, 720);
            Initialize_background();
            Initialize_sprite();
            timer1.Tick += new EventHandler(timer1_Tick);
            this.KeyDown += new KeyEventHandler(keysdown);
        }

        private void Initialize_background()
        {
            Background_ingame1 = Run_and_Kills.Properties.Resources.background;
            Background_ingame2 = Run_and_Kills.Properties.Resources.background;
            Background1_ingame1 = Run_and_Kills.Properties.Resources.background1;
            Background1_ingame2 = Run_and_Kills.Properties.Resources.background1;
            Background2_ingame1 = Run_and_Kills.Properties.Resources.background2;
            Background2_ingame2 = Run_and_Kills.Properties.Resources.background2;
            Background3_ingame1 = Run_and_Kills.Properties.Resources.background3;
            Background3_ingame2 = Run_and_Kills.Properties.Resources.background3;
            BackgroundLane_ingame1 = Run_and_Kills.Properties.Resources.background_main;
            BackgroundLane_ingame2 = Run_and_Kills.Properties.Resources.background_main;
            Rec_background_ingame1 = new Rectangle(0, 0, 1102, 720);
            Rec_background_ingame2 = new Rectangle(1080, 0, 1102, 720);
            Rec_background1_ingame1 = new Rectangle(0, -200, 1080, 720);
            Rec_background1_ingame2 = new Rectangle(1080, -200, 1080, 720);
            Rec_background2_ingame1 = new Rectangle(0, -265, 1080, 720);
            Rec_background2_ingame2 = new Rectangle(1080, -265, 1080, 720);
            Rec_background3_ingame1 = new Rectangle(0, -260, 1080, 720);
            Rec_background3_ingame2 = new Rectangle(1080, -260, 1080, 720);
            Rec_backgroundLane_ingame1 = new Rectangle(0, -275, 1082, 720);
            Rec_backgroundLane_ingame2 = new Rectangle(1080, -275, 1082, 720);
        }

        private void Initialize_sprite()
        {
            backBuffer_run = new Bitmap(70, 86);
            sprite_run = new Bitmap(Run_and_Kills.Properties.Resources.run);
            sprite_jump = new Bitmap(Run_and_Kills.Properties.Resources.jump);
            sprite_360 = new Bitmap(Run_and_Kills.Properties.Resources.jump_360);
            //bullet = new Bitmap(Run_and_Kills.Properties.Resources.Bullet);
            index_sprite = 0;
            graphics_run = this.CreateGraphics();
        }

        private void Button_play_Click(object sender, EventArgs e)
        {

            Background_Play.Hide();
            timer1.Start();
            //Invalidate();
        }

        private void Run_and_Kill_Load(object sender, EventArgs e)
        {

        }

        private void Render()
        {

            Graphics g;
            g = Graphics.FromImage(backBuffer_run);
            g.Clear(Color.Transparent);
            curFrameColumn_run = index_sprite % 5;// 530;
            curFrameRow_run = index_sprite / 5;//710;

            if (bossTimer != -1)
            {
                g.DrawImage(sprite_jump, 0, 0, new Rectangle(curFrameColumn_run * 70, curFrameRow_run * 86, 70, 86), GraphicsUnit.Pixel);

            }
            else if (bossTimer < 10)
                g.DrawImage(sprite_run, 0, 0, new Rectangle(curFrameColumn_run * 70, curFrameRow_run * 86, 70, 86), GraphicsUnit.Pixel);
            else if (bossTimer < 15)
            {
                g.DrawImage(sprite_360, 0, 0, new Rectangle(0, 0, 70, 86), GraphicsUnit.Pixel);
            }
            else
                g.DrawImage(sprite_360, 0, 0, new Rectangle(0, 0, 70, 86), GraphicsUnit.Pixel);

            g.Dispose();
            
            if (index_sprite > 8)
            {
                index_sprite = 0;
            }
            else
                index_sprite++;

        }

        private void Load_background(Graphics g)
        {
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            // Toc do Troi
            Rec_background_ingame1.X -= 0;
            Rec_background_ingame2.X -= 0;
            if (Rec_background_ingame1.X <= -1080) Rec_background_ingame1.X = 1080;
            if (Rec_background_ingame2.X <= -1080) Rec_background_ingame2.X = 1080;
            g.DrawImage(Background_ingame1, Rec_background_ingame1);
            g.DrawImage(Background_ingame2, Rec_background_ingame2);
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            //Toc do May
            Rec_background1_ingame1.X -= 1;
            Rec_background1_ingame2.X -= 1;
            if (Rec_background1_ingame1.X <= -1080) Rec_background1_ingame1.X = 1080;
            if (Rec_background1_ingame2.X <= -1080) Rec_background1_ingame2.X = 1080;
            g.DrawImage(Background1_ingame1, Rec_background1_ingame1);
            g.DrawImage(Background1_ingame2, Rec_background1_ingame2);

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            // Toc do Nui
            Rec_background2_ingame1.X -= 4;
            Rec_background2_ingame2.X -= 4;
            if (Rec_background2_ingame1.X <= -1080) Rec_background2_ingame1.X = 1080;
            if (Rec_background2_ingame2.X <= -1080) Rec_background2_ingame2.X = 1080;
            g.DrawImage(Background2_ingame1, Rec_background2_ingame1);
            g.DrawImage(Background2_ingame2, Rec_background2_ingame2);

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            // Toc do Cay
            Rec_background3_ingame1.X -= 20;
            Rec_background3_ingame2.X -= 20;
            if (Rec_background3_ingame1.X <= -1080) Rec_background3_ingame1.X = 1080;
            if (Rec_background3_ingame2.X <= -1080) Rec_background3_ingame2.X = 1080;
            g.DrawImage(Background3_ingame1, Rec_background3_ingame1);
            g.DrawImage(Background3_ingame2, Rec_background3_ingame2);

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            // Toc do Lane
            Rec_backgroundLane_ingame1.X -= 30;
            Rec_backgroundLane_ingame2.X -= 30;
            if (Rec_backgroundLane_ingame1.X <= -1080) Rec_backgroundLane_ingame1.X = 1080;
            if (Rec_backgroundLane_ingame2.X <= -1080) Rec_backgroundLane_ingame2.X = 1080;
            g.DrawImage(BackgroundLane_ingame1, Rec_backgroundLane_ingame1);
            g.DrawImage(BackgroundLane_ingame2, Rec_backgroundLane_ingame2);

        }

        int dem_jump = 250;
        int dem_jump_du = 0;
        private void Location_sprite(Graphics g)
        {
            if (bossTimer == -1)
            {
                dem_jump = 250;
                g.DrawImageUnscaled(backBuffer_run, 250, dem_jump);

            }
            else if (bossTimer < 5)
            {
                dem_jump -= 20;
                g.DrawImage(backBuffer_run, 250, dem_jump);
            }
            else if (bossTimer < 10)
            {
                dem_jump += 20;
                g.DrawImage(backBuffer_run, 250, dem_jump);
            }
            else if (bossTimer < 15)
            {
                dem_jump -= 20;
                if (bossTimer == 11) backBuffer_run.RotateFlip(RotateFlipType.Rotate90FlipX);
                if (bossTimer == 12) backBuffer_run.RotateFlip(RotateFlipType.Rotate180FlipX);
                if (bossTimer == 13) backBuffer_run.RotateFlip(RotateFlipType.Rotate270FlipX);
                g.DrawImage(backBuffer_run, 250, dem_jump);
            }
            else if (bossTimer <= 19 + dem_jump_du)
            {
                dem_jump += 20;
                g.DrawImage(backBuffer_run, 250, dem_jump);
            }
        }
        private void Type_sprite()
        {
            if (bossTimer != -1) bossTimer++;
            if (bossTimer == 10) bossTimer = -1;
            if (bossTimer == 20 + dem_jump_du) bossTimer = -1;
        }
        private void add_bullet()
        {
            bullet = new Bullet();
            if ((bossTimer>-1 && bossTimer <5) || (bossTimer <15 && bossTimer >10)) bullet.set_location(310, dem_jump+35);
            else if ((bossTimer >= 5 && bossTimer <=10) || (bossTimer >= 15 && bossTimer <=19)) bullet.set_location(310, dem_jump+70);
            else bullet.set_location(310, dem_jump + 35);
            bullets.Add(bullet);
        }
        private void Render_bullet()
        {
            Jump.Text = bullets.Count().ToString();
            for (int i=0; i< bullets.Count(); i++)
            {
                Fire.Text = i.ToString();
                if (bullets[i].get_x() > 900 && bullets != null) bullets.RemoveAt(i);
            }
        }
        int dem_timer = 0;
        int timer_bullet = 3;
        private void timer1_Tick(object sender, EventArgs e)
        {
            dem_timer++;
            Graphics g;
            g = Graphics.FromImage(All);
            g.Clear(Color.Transparent);
            Load_background(g);
                            
            Render();
            Render_bullet();
            Location_sprite(g);
            Type_sprite();
            //int i = bullets.Count();
            foreach (Bullet bullet in bullets)
            {
                g.DrawImage(bullet.get_img(), bullet.get_x(), bullet.get_y());
                bullet.increase_location();
            }
            graphics_run.DrawImageUnscaled(All, 0, 0);
            g.Dispose();
            if (dem_timer == 10) dem_timer = 0;
            timer_bullet++;
        }



        private void keysdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F)
            {
                if (timer_bullet > 3) {
                    add_bullet();
                    timer_bullet = 0;
                }        
            }
            if (e.KeyCode == Keys.Space || e.KeyCode == Keys.Up)
            {
                if (bossTimer == -1)
                {
                    bossTimer = 0;
                    index_sprite = 0;
                }
                else if (bossTimer < 10)
                {
                    if (bossTimer < 5)
                        dem_jump_du = bossTimer - 1;
                    else
                        dem_jump_du = 4 - (bossTimer - 5);
                    bossTimer = 11;

                }

            }
            
        }
    }
}
